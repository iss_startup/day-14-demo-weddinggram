var Sequelize = require("sequelize");

module.exports = function (database) {
    return database.define('user', {
        username: Sequelize.STRING,
        password: Sequelize.STRING,
        firstName: Sequelize.STRING,
        lastName: Sequelize.STRING,
        addressLine1: Sequelize.STRING,
        addressLine2: Sequelize.STRING,
        city: Sequelize.STRING,
        postcode: Sequelize.STRING,
        phone: Sequelize.STRING
    });
};